export function saveItemToLocalStorage(key, value) {
  localStorage.setItem(key, value);
}

export function getItemFromLocalStorage(key) {
  return localStorage.getItem(key);
}

export function removeItemFromLocalStorage(key) {
  localStorage.removeItem(key);
}

export async function fetchSessionData(id, setSession) {
  fetch(`/api/sessions/${id}`)
    .then((res) => res.json())
    .then((data) => {
      setSession(data);
      saveItemToLocalStorage("session", JSON.stringify(data));
    });
}

export function reanderParticipantsVideo(
  stream,
  user,
  peerId,
  muted = false,
  currentUser = false,
  cardWrapperSelector = ".participants-video",
  appendChild = true
) {
  const videoCardWrapper = document.createElement("div");
  const videoCardEl = document.createElement("div");
  videoCardEl.dataset.peerId = peerId;
  videoCardEl.className = `${currentUser ? "me" : ""}`;
  const participantVideo = document.createElement("video");
  participantVideo.srcObject = stream;
  // participantVideo.transferControlToOffscreen()
  // participantVideo.style.display = 'none';
  participantVideo.autoplay = true;
  participantVideo.muted = muted;
  const participantFullNameWrapper = document.createElement("section");
  participantFullNameWrapper.className = "full-name-wrapper";
  participantFullNameWrapper.innerHTML = `<span>${user.name}</span>`;

  const participantAudioWrapper = document.createElement("section");
  participantAudioWrapper.className = "participant-audio-status";
  participantAudioWrapper.innerHTML = getAudioIcon(muted);

  const participantShortNameWrapper = document.createElement("section");
  participantShortNameWrapper.className = "name-wrapper";
  participantShortNameWrapper.innerHTML = `<span>${user.name.substring(
    0,
    1
  )}</span>`;

  stream.getTracks().forEach((track) => {
    console.log(track);
    if (track.kind === "video" && !track.enabled) {
      participantShortNameWrapper.style.visibility = "visible";
      participantVideo.style.display = "none";
    }
  });

  videoCardEl.appendChild(participantVideo);
  videoCardWrapper.appendChild(participantAudioWrapper);
  videoCardWrapper.appendChild(participantFullNameWrapper);
  videoCardWrapper.appendChild(participantShortNameWrapper);
  videoCardWrapper.appendChild(videoCardEl);

  const cardWrapper = document.querySelector(cardWrapperSelector);

  if (appendChild) {
    cardWrapper.appendChild(videoCardWrapper);
  } else {
    cardWrapper.childNodes.forEach((child) => {
      child.remove();
    });

    cardWrapper.appendChild(videoCardWrapper);
  }
}

export function getAudioIcon(muted) {
  if (!muted) {
    return '<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M13 8c0 .564-.094 1.107-.266 1.613l-.814-.814A4.02 4.02 0 0 0 12 8V7a.5.5 0 0 1 1 0v1zm-5 4c.818 0 1.578-.245 2.212-.667l.718.719a4.973 4.973 0 0 1-2.43.923V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 1 0v1a4 4 0 0 0 4 4zm3-9v4.879l-1-1V3a2 2 0 0 0-3.997-.118l-.845-.845A3.001 3.001 0 0 1 11 3z"></path><path d="m9.486 10.607-.748-.748A2 2 0 0 1 6 8v-.878l-1-1V8a3 3 0 0 0 4.486 2.607zm-7.84-9.253 12 12 .708-.708-12-12-.708.708z"></path></svg>';
  }

  return '<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M3.5 6.5A.5.5 0 0 1 4 7v1a4 4 0 0 0 8 0V7a.5.5 0 0 1 1 0v1a5 5 0 0 1-4.5 4.975V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 .5-.5z"></path><path d="M10 8a2 2 0 1 1-4 0V3a2 2 0 1 1 4 0v5zM8 0a3 3 0 0 0-3 3v5a3 3 0 0 0 6 0V3a3 3 0 0 0-3-3z"></path></svg>';
}

export function isParticipantVideoExists(peerId) {
  const videoContainerEl = document.querySelector(".participants-video");
  const videoCard = videoContainerEl.querySelector(
    `div[data-peer-id="${peerId}"]`
  );

  if (videoCard) return true;
}

export function remoteVideoCard(socketId) {
  const videoContainerEl = document.querySelector(".participants-video");
  const videoCard = videoContainerEl.querySelector(
    `div[data-peer-id="${socketId}"]`
  )
    ? videoContainerEl.querySelector(`div[data-peer-id="${socketId}"]`)
        .parentNode
    : null;
  if (videoCard) videoCard.remove();
}

export const coptyToClipbord = (link) => {
  navigator.clipboard.writeText(link).then(
    () => {},
    () => {
      /* clipboard write failed */
    }
  );
};

export const findElementBySelector = (selector) => {
  return document.querySelector(selector);
};

export const fillParticipantShortName = (info) => {
  let participantShortNameEl = findElementBySelector(
    `div[data-peer-id="${info.callerId}"]`
  ).parentNode.querySelector(".name-wrapper");

  let participantFullNameEl = findElementBySelector(
    `div[data-peer-id="${info.callerId}"]`
  ).parentNode.querySelector(".full-name-wrapper");

  console.log(info, participantShortNameEl);

  participantFullNameEl.innerText = info.callerName;
  participantShortNameEl.innerText = info.callerName.substring(0, 1);
};

export const changeCallerStreamConstraints = (info) => {
  let participantVideoEl = findElementBySelector(
    `div[data-peer-id="${info.callerId}"] video`
  );

  const remoteStream = participantVideoEl.srcObject;
  remoteStream.getTracks().forEach((track) => {
    if (track.kind === "audio") {
      track.enabled = info.callerConstraints.audio;
    }

    if (track.kind === "video") {
      track.enabled = info.callerConstraints.video;
    }
  });

  participantVideoEl.srcObject = remoteStream;
  toggleParticipantVideo(
    info.callerConstraints,
    `div[data-peer-id="${info.callerId}"]`
  );
};

export const toggleParticipantVideo = (constriants, videoWrapperSelector) => {
  console.log(videoWrapperSelector);
  const videoEl = findElementBySelector(`${videoWrapperSelector} video`);
  const nameWrapperEl =
    findElementBySelector(videoWrapperSelector).parentNode.querySelector(
      ".name-wrapper"
    );

  if (!constriants.video) {
    videoEl.style.display = "none";
    nameWrapperEl.style.visibility = "visible";
  } else {
    videoEl.style.display = "";
    nameWrapperEl.style.visibility = "hidden";
  }
};

export const findSessionHostSocketId = (participants, hostId) => {
  console.log(participants, hostId);
  const host = participants.find(
    (participant) => parseInt(participant.user.id) === parseInt(hostId)
  );

  if (host) {
    return host;
  }

  return null;
};

export const changeView = (setView, participants, session) => {
  let view = "";
  switch (participants.length) {
    case 1:
      view = "one";
      break;
    case 2:
      view =   "one-to-one";
      break;
    default:
      view = "gallery";
  }

  setView(view);

  // if (view === "speaker") {
  //   cloneHostVideoCard(participants, session);
  //   console.log("dda");
  // }
};

export const cloneHostVideoCard = (parts, session) => {
  const host = findSessionHostSocketId(parts, session.host.id);
  const hostVideoEl = findElementBySelector(
    `div[data-peer-id="${host.socketId}"] video`
  );

  console.log(hostVideoEl);

  if (hostVideoEl) {
    const hostStream = hostVideoEl.srcObject;
    reanderParticipantsVideo(
      hostStream,
      host.user,
      host.socketId,
      false,
      false,
      ".hero-stream",
      false
    );
  }
};

export const renderSharedScreen = (userInfo, stream, peerId) => {
  const videoCardWrapper = document.createElement("div");
  const videoCardEl = document.createElement("div");
  videoCardWrapper.dataset.peerId = peerId;
  const participantVideo = document.createElement("video");
  participantVideo.srcObject = stream;
  participantVideo.autoplay = true;

  videoCardEl.appendChild(participantVideo);
  videoCardWrapper.appendChild(videoCardEl);

  const shareScreenWrapper = document.querySelector(".hero-stream");
  shareScreenWrapper.appendChild(videoCardWrapper);
};

export const shareScreenEnded = (shareScreenSocketId) => {
  const sharedScreenWrapper = document.querySelector(
    `.hero-stream div[data-peer-id="${shareScreenSocketId}"]`
  );

  if (sharedScreenWrapper) {
    sharedScreenWrapper.remove();
  }
};
