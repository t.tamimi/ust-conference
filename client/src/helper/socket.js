import {
  reanderParticipantsVideo,
  isParticipantVideoExists,
  findElementBySelector,
  toggleParticipantVideo,
  getAudioIcon,
  renderSharedScreen,
} from "./utils";

export const answerCall = (call, constriants, user, peerId) => {
  navigator.mediaDevices
    .getUserMedia({
      video: true,
      audio: {
        echoCancellation: true,
        noiseSuppression: true,
      }
    })
    .then((stream) => {
      // stream.getTracks().forEach((track) => {
      //   if (track.kind === "audio") {
      //     track.enabled = constriants.audio;
      //   }

      //   if (track.kind === "video") {
      //     track.enabled = constriants.video;
      //   }
      // });

      // console.log(stream.getTracks())

      call.answer(stream);
      call.on("stream", (remoteStream) => {
        // Show stream in some <video> element.
        if (!isParticipantVideoExists(peerId)) {
          console.log("recived call ....");
          console.log(remoteStream.getTracks());

          reanderParticipantsVideo(remoteStream, user, peerId);
        }
      });

      call.on("close", (err) => {
        console.log("Close : " + err);
      });

      call.on("error", (err) => {
        console.log("ERROR : " + err);
      });
    });
};

export const controlUserAccess = (session, user, remoteUser, socket) => {
  console.log(`USER : ${remoteUser.id}`);

  if (session.host.id === user.id) {
    // If user is the host
    if (window.confirm(`User ${remoteUser.name} is asking to join ...`)) {
      socket.current.emit(
        "ask-permission-to-join-answer",
        true,
        remoteUser.id,
        session._id
      );
    } else {
      socket.current.emit(
        "ask-permission-to-join-answer",
        false,
        remoteUser.id,
        session._id
      );
    }
  }
};

export const call = (remotePeerId, constriants, myPeer, user, socket) => {
  console.log("new user connected " + remotePeerId);
  // const callerStream = getMediaStream(constriants)
  navigator.mediaDevices
    .getUserMedia({
      video: true,
      audio: {
        echoCancellation: true,
        noiseSuppression: true,
      }
    })
    .then((stream) => {
      // stream.getTracks().forEach((track) => {
      //   if (track.kind === "audio") {
      //     track.enabled = constriants.audio;
      //   }

      //   if (track.kind === "video") {
      //     track.enabled = constriants.video;
      //   }
      // });

      const call = myPeer.call(remotePeerId, stream);
      console.log(call);
      call.on("stream", function (remoteStream) {
        // Show stream in some video/canvas element.
        if (!isParticipantVideoExists(remotePeerId)) {
          console.log("called some one ");
          console.log(remoteStream.getTracks());
          reanderParticipantsVideo(remoteStream, user, remotePeerId);
          socket.current.emit("change-caller-info", remotePeerId, constriants);
        }
      });

      call.on("error", (err) => {
        console.log("ERROR : " + err);
      });

      call.on("close", (err) => {
        console.log("Close : " + err);
      });
    });
};

export const changePeerStream = (constriants, peerId) => {
  let participantAudioWrapper = findElementBySelector(
    `div[data-peer-id="${peerId}"]`
  ).parentNode.querySelector(".participant-audio-status");

  const videoEl = findElementBySelector(`div[data-peer-id="${peerId}"] video`);
  const remoteStream = videoEl.srcObject;
  remoteStream.getTracks().forEach((track) => {
    if (track.kind === "video") {
      track.enabled = constriants.video;
    } else {
      track.enabled = constriants.audio;
      participantAudioWrapper.innerHTML = getAudioIcon(constriants.audio);
    }
  });

  videoEl.srcObject = remoteStream;

  toggleParticipantVideo(constriants, `div[data-peer-id="${peerId}"]`);

  console.log(constriants);
};

export const leaveSession = (myPeer, socket, myStream, navigate) => {
  myPeer.destroy();
  socket.current.disconnect();

  document.querySelectorAll("video").forEach((video) => {
    video.pause();
    video.srcObject.getTracks().forEach((track) => track.stop());
    video.srcObject = null;
  });

  myStream.current = null;

  navigate("/session/exit", { replace: true });
};

export const callShareSocket = (remoteSharePeerId, remoteUser, myPeer, setView) => {
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: false })
    .then((stream) => {
      const call = myPeer.call(remoteSharePeerId, stream);
      call.on("stream", (stream) => {
        setView('screen-share')
        renderSharedScreen(remoteUser, stream, remoteSharePeerId);
      });

      call.on('error', () => {
        alert('err')
      })
    });
};
