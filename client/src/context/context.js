import React, { useContext, useRef, useState } from "react";

import * as Utils from "../helper/utils";

const AppContext = React.createContext();
const AppProvider = ({ children }) => {
  const [session, setSession] = useState(
    JSON.parse(Utils.getItemFromLocalStorage("session")) || {}
  );

  const [constriants, setConstriants] = useState(
    JSON.parse(Utils.getItemFromLocalStorage("constriants")) || {
      video: false,
      audio: false,
    }
  );

  const [user, setUser] = useState(JSON.parse(Utils.getItemFromLocalStorage('user')) || {id: 0 , name: ''});
  const URL = process.env.NODE_ENV === 'production' ? undefined : 'http://localhost:8000';
  const [participants, setParticipants] = useState([])
  let socket  = useRef();
  const [messages, setMessages] = useState([]);
  const myStream = useRef();
  const [isSideOpen, setSideOpen] = useState(false);
  const participantTab = useRef();
  const chatTab = useRef();
  const participantContainerTab = useRef();
  const chatContainerTab = useRef();
  const myLocalPeer = useRef();
  const [view, setView] = useState('');
  const [screenShareList, setScreenShareList] = useState([]);

  return (
    <AppContext.Provider
      value={{
        session,
        setSession,
        constriants,
        setConstriants,
        user,
        setUser,
        URL,
        participants,
        setParticipants,
        socket,
        messages,
        setMessages,
        myStream,
        isSideOpen,
        setSideOpen,
        participantContainerTab,
        participantTab,
        chatContainerTab,
        chatTab,
        myLocalPeer,
        view,
        setView,
        screenShareList,
        setScreenShareList
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppProvider, useGlobalContext };
