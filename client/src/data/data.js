import { BsCameraVideo, BsCameraVideoOff, BsChatSquareDots, BsMic, BsMicMute, BsRecordCircle } from "react-icons/bs";
import {TbScreenShare} from 'react-icons/tb';
// import {} from 'react-icons/bs';
import {HiOutlineUserGroup} from 'react-icons/hi'

export const controlButtons = [
    {
        id: 1,
        Icon: BsMic,
        bgColor: '',
        color: ''
    },
    {
        id:2,
        Icon: BsMicMute,
        bgColor: '',
        color: '',
        display: 'none'
    },
    {
        id:3,
        Icon: BsCameraVideo,
        bgColor: '',
        color: ''
    },
    {
        id: 4,
        Icon: BsCameraVideoOff,
        bgColor: '',
        color: '',
        display: 'none'
    },
    {
        id:5,
        Icon: TbScreenShare,
        bgColor: '',
        color: ''
    },
    {
        id:6,
        Icon: HiOutlineUserGroup,
        bgColor: '',
        color: ''
    },
    {
        id:7,
        Icon: BsChatSquareDots,
        bgColor: '',
        color: ''
    },
    {
        id: 8,
        Icon: BsRecordCircle,
        bgColor: '',
        color: '',
        display: ''
    },
]