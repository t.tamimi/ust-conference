import React, { useState, useRef, useEffect } from "react";
import {
  BsFillPersonFill,
  BsFillCameraVideoFill,
  BsFillCameraVideoOffFill,
} from "react-icons/bs";
import { FaMicrophoneSlash, FaMicrophone } from "react-icons/fa";
import { useParams, useNavigate, useSearchParams } from "react-router-dom";
import NotFound from "../components/NotFound";
import { useGlobalContext } from "../context/context";
import { io } from "socket.io-client";
import { fetchSessionData, saveItemToLocalStorage } from "../helper/utils";

export default function SetupSessionPage() {
  const {
    session,
    setSession,
    constriants,
    setConstriants,
    user,
    setUser,
    URL,
  } = useGlobalContext();
  const [stream, setStream] = useState();
  const videoRef = useRef();
  const controlesRef = useRef();
  const avaterRef = useRef();
  const navigate = useNavigate();
  let [searchParams] = useSearchParams();

  const { id } = useParams();

  const askUserStream = async (mediaType) => {
    let localConstraints = {};

    if (mediaType === "video") {
      localConstraints = { ...constriants, video: !constriants.video };
      setTracks(localConstraints);
      setConstriants(localConstraints);
      console.log(localConstraints);
    }

    if (mediaType === "audio") {
      localConstraints = { ...constriants, audio: !constriants.audio };
      setTracks(localConstraints);
      setConstriants(localConstraints);
      console.log(localConstraints);
    }

    if (localConstraints.video) {
      videoRef.current.style.display = "block";
      controlesRef.current.className = "controls-wrapper floated";
      chanageTrackIcon("video", "on");
    } else {
      videoRef.current.style.display = "none";
      controlesRef.current.className = "controls-wrapper";
      chanageTrackIcon("video", "off");
    }

    if (localConstraints.audio) {
      if (!localConstraints.video) {
        changeAvaterIcon("audio", "on");
      }
      chanageTrackIcon("audio", "on");
    } else {
      chanageTrackIcon("audio", "off");
      changeAvaterIcon("audio", "off");
    }

    saveItemToLocalStorage("constriants", JSON.stringify(localConstraints));
  };

  const setTracks = (constriants) => {
    if (!constriants.video && !constriants.audio) {
      removeTracks();
      if (!constriants.video) {
        videoRef.current.style.display = "none";
        controlesRef.current.className = "controls-wrapper";
      }
      return;
    }

    navigator.mediaDevices.getUserMedia(constriants).then((str) => {
      videoRef.current.srcObject = str;
      videoRef.current.autoplay = true;

      setStream(str);
    });
  };

  const removeTracks = () => {
    stream.getTracks().forEach((track) => {
      track.stop();
    });

    videoRef.current.srcObject = stream;

    setStream(null);
  };

  const changeAvaterIcon = (mediaType, status) => {
    if (status === "on") {
      avaterRef.current.querySelector("svg:first-child").style.display = "none";
      avaterRef.current.querySelector("svg:last-child").style.display = "block";
    } else {
      avaterRef.current.querySelector("svg:first-child").style.display =
        "block";
      avaterRef.current.querySelector("svg:last-child").style.display = "none";
    }
  };

  const chanageTrackIcon = (mediaType, status) => {
    if (mediaType === "audio") {
      if (status === "on") {
        controlesRef.current.querySelector(
          ".controls-element:first-child svg:nth-child(1)"
        ).style.display = "none";
        controlesRef.current.querySelector(
          ".controls-element:first-child svg:nth-child(2)"
        ).style.display = "block";
      } else {
        controlesRef.current.querySelector(
          ".controls-element:first-child svg:nth-child(1)"
        ).style.display = "block";
        controlesRef.current.querySelector(
          ".controls-element:first-child svg:nth-child(2)"
        ).style.display = "none";
      }
    } else {
      if (status === "on") {
        controlesRef.current.querySelector(
          ".controls-element:last-child svg:nth-child(1)"
        ).style.display = "none";
        controlesRef.current.querySelector(
          ".controls-element:last-child svg:nth-child(2)"
        ).style.display = "block";
      } else {
        controlesRef.current.querySelector(
          ".controls-element:last-child svg:nth-child(1)"
        ).style.display = "block";
        controlesRef.current.querySelector(
          ".controls-element:last-child svg:nth-child(2)"
        ).style.display = "none";
      }
    }
  };

  const handelClick = () => {
    if (session && session._id && stream) {

      if (
        session.sesstionType.toLowerCase() === "normal" &&
        session.host.id !== user.id
      ) {
        const socket = io(URL, {
          auth: {
            sessionId: session._id,
            user,
          },
        });

        socket.emit("ask-permission-to-join", user, session._id);

        socket.on("ask-permission-to-join-answer", (status, userId) => {
          if (socket.auth.user.id === userId) {
            if (status) {
              removeTracks();
              navigate(`/session/${session._id}`);
            } else {
              alert("not allowed to join...");
            }
          }
        });
        return;
      }

      removeTracks();

      navigate(`/session/${session._id}`);
    }
  };

  useEffect(() => {
    fetchSessionData(id, setSession);

    if (constriants.video || constriants.audio) {
      setConstriants({ ...constriants, video: false, audio: false });
    }

    let userId = 0;
    let name = "";

    if (searchParams.get("id") && searchParams.get("name")) {
      userId = searchParams.get("id");
      name = searchParams.get("name");
    } else {
      if (session.sesstionType.toLowerCase() === "normal") {
        while (userId === 0 && name === "") {
          userId = window.prompt("ادخل رقمك الوظيفي") || 0;
          name = window.prompt("ادخل اسمك") || "";
        }
      }
    }

    setUser({...user,id: userId,name,});
    saveItemToLocalStorage(
      "user",
      JSON.stringify({
        id: userId,
        name,
      })
    );
  }, []);

  return (
    <React.Fragment>
      {session && session._id ? (
        <section className="setup-wrapper">
          <article className="content">
            <header>Join Meeting</header>
            <article className="video-wrapper">
              <video ref={videoRef} style={{ objectFit: "fill" }}></video>
              <div>
                <div className="icon-wrapper" ref={avaterRef}>
                  <BsFillPersonFill
                    style={{ fontSize: "7rem", color: "rgb(100 96 96)" }}
                  />
                  <FaMicrophone
                    style={{
                      fontSize: "7rem",
                      color: "rgb(100 96 96)",
                      display: "none",
                    }}
                  />
                </div>
                <div className="controls-wrapper" ref={controlesRef}>
                  <div
                    className="controls-element"
                    title="Start Audio"
                    onClick={() => askUserStream("audio")}
                  >
                    <FaMicrophoneSlash style={{ fontSize: "1.3rem" }} />
                    <FaMicrophone
                      style={{ display: "none", fontSize: "1.3rem" }}
                    />
                    <span>Start Audio</span>
                  </div>
                  <div
                    className="controls-element"
                    title="Start Video"
                    onClick={() => askUserStream("video")}
                  >
                    <BsFillCameraVideoOffFill style={{ fontSize: "1.3rem" }} />
                    <BsFillCameraVideoFill
                      style={{ display: "none", fontSize: "1.3rem" }}
                    />
                    <span>Start Video</span>
                  </div>
                </div>
              </div>
            </article>
            <button
              type="button"
              className={`${
                !stream || user.id === 0 || user.name === "" ? "disabled" : ""
              }`}
              disabled={!stream || user.id === 0 || user.name === ""}
              onClick={handelClick}
            >
              JOIN
            </button>
          </article>
        </section>
      ) : (
        <NotFound />
      )}
    </React.Fragment>
  );
}
