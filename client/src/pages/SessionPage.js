import React, { useEffect } from "react";
import { useGlobalContext } from "../context/context";
import { useParams, useNavigate } from "react-router-dom";
import { io } from "socket.io-client";
import Peer from "peerjs";
import {
  answerCall,
  call,
  callShareSocket,
  changePeerStream,
  controlUserAccess,
  leaveSession,
} from "../helper/socket";
import {
  reanderParticipantsVideo,
  remoteVideoCard,
  fillParticipantShortName,
  findElementBySelector,
  changeView,
  changeCallerStreamConstraints,
  shareScreenEnded,
} from "../helper/utils";
import { ConfHeader } from "../components/ConfHeader";
import { ConfSidebar } from "../components/ConfSidebar";
import { ConfContent } from "../components/ConfContent";

export default function SessionPage() {
  const {
    session,
    constriants,
    URL,
    user,
    participants,
    setParticipants,
    socket,
    setMessages,
    myStream,
    myLocalPeer,
    view,
    setView,
    setScreenShareList,
    screenShareList,
  } = useGlobalContext();

  const navigate = useNavigate();

  useEffect(() => {
    // fetchSessionData(id, setSession);
    socket.current = io(URL, {
      auth: {
        sessionId: session._id,
        user,
        isShareScreenSocket: false
      },
    });

    socket.current.on("connect", () => {
      const myPeer = new Peer(socket.current.id, {
        path: "/peerjs",
        host: "/",
        port: "8000",
      });

      navigator.mediaDevices
        .getUserMedia({ video: true, audio: true })
        .then((str) => {
          str.getTracks().forEach((track) => {
            if (track.kind === "audio") {
              track.enabled = constriants.audio;
            }

            if (track.kind === "video") {
              track.enabled = constriants.video;
            }
          });

          myStream.current = str;
          console.log(str.getTracks());

          reanderParticipantsVideo(str, user, myPeer.id, true, true);
        });

      myPeer.on("call", (call) => {
        console.log(participants);
        answerCall(call, constriants, user, call.peer);
      });

      myPeer.on("open", (myPeerId) => {
        socket.current.emit("user-connected", myPeerId, constriants);
        myLocalPeer.current = myPeer;
      });

      socket.current.on("ask-permission-to-join", (remoteUser) => {
        controlUserAccess(session, user, remoteUser, socket);
      });

      socket.current.on("user-connected", (remotePeerId, remoteUser) => {
        console.log(participants);
        call(remotePeerId, constriants, myPeer, remoteUser, socket);
      });

      socket.current.on("change-caller-info", (info) => {
        if (info.recipentId === socket.current.id) {
          fillParticipantShortName(info);
          changeCallerStreamConstraints(info);
          console.log(participants);
        } else {
          console.log(info, socket.current.id);
        }
      });

      socket.current.on("user-disconnected", (socketId) => {
        console.log(socketId);
        remoteVideoCard(socketId);
      });

      socket.current.on("participants-list-changed", (parts) => {
        console.log(parts);
        setParticipants(parts);
        console.log(screenShareList)
        changeView(setView, parts, session);
      });

      socket.current.on("new-message", (messages) => {
        setMessages(messages);
      });

      socket.current.on("stream-changed", (constriants, socketId) => {
        console.log(constriants);
        changePeerStream(constriants, socketId);
      });

      socket.current.on("session-ended", (id) => {
        if (parseInt(session.host.id) === parseInt(id)) {
          leaveSession(myPeer, socket, myStream, navigate);
        }

        if (parseInt(id) === parseInt(user.id)) {
          leaveSession(myPeer, socket, myStream, navigate);
        }
      });

      socket.current.on("share-screen", (remoteShareSocketId, remoteUser, screenShares) => {
        // if (parseInt(remoteUser.id) !== parseInt(user.id)) {
          setScreenShareList(screenShares);

          console.log(screenShares)

          callShareSocket(remoteShareSocketId, remoteUser, myPeer, setView);
        // }
      });

      socket.current.on("screen-share-ended", (shareScreenSocketId, parts, screenShares) => {
        console.log(shareScreenSocketId, '0000000000')

        if(screenShares.length === 0) {
          console.log(participants);
          changeView(setView, parts, session)
        }

        console.log(screenShares)

        setScreenShareList(screenShares);
        shareScreenEnded(shareScreenSocketId)
      });

      socket.current.on("screen-share-canceled", (screenShares) => {
        setScreenShareList(screenShares);
      })
    });
  }, []);

  return (
    <section className="body-wrapper">
      <ConfHeader />
      <ConfContent />
    </section>
  );
}
