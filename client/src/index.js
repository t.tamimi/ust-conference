import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import "./one-to-one.css";
import "./speaker-view.css";
import "./gallery-view.css";
import "./share-screen-view.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import SetupSessionPage from "./pages/SetupSessionPage";
import SessionPage from "./pages/SessionPage";
import SessionExitPage from "./pages/SessionExitPage";
import NotFound from "./components/NotFound";
import { AppProvider } from "./context/context";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
    <AppProvider>
      <Router>
        <Routes>
          <Route exact path="/:id" element={<SetupSessionPage />} />
          <Route path="/session/:id" element={<SessionPage />} />
          <Route path="/session/exit" element={<SessionExitPage />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Router>
    </AppProvider>
  // </React.StrictMode>
);


