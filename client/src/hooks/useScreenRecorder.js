import React, { useState } from "react";

export default function useScreenRecorder(props) {

  async function recordScreen() {
    return await navigator.mediaDevices.getDisplayMedia({
      audio: false,
      video: { displaySurface: "monitor" },
    });
  }

  function createRecorder(stream) {
    let mediaRecorder = new MediaRecorder(stream);
    let recordedChunks = [];

    mediaRecorder.ondataavailable = function (e) {
      if (e.data.size > 0) {
        recordedChunks.push(e.data);

        if(props.cloudRecording) {
            const blob = new Blob(recordedChunks, { type: 'video/mp4' });
            props.socket.current.emit('new-video-chunk', blob);
        }
      }
    };
    mediaRecorder.onstop = function () {
      
      if(!props.cloudRecording) {
        saveFile(recordedChunks);
      }
      recordedChunks = [];
    };

    mediaRecorder.start(1000); // For every 1000ms the stream data will be stored in a separate chunk.
  }

  function saveFile(recordedChunks) {
    const blob = new Blob(recordedChunks, { type: 'video/mp4' });

    let downloadLink = document.createElement("a");
    downloadLink.href = URL.createObjectURL(blob);
    downloadLink.download = `${props.filename}.mp4`;

    document.body.appendChild(downloadLink);
    downloadLink.click();
    URL.revokeObjectURL(blob); // clear from memory
    document.body.removeChild(downloadLink);
  }

  async function startRecording() {
    const stream = await recordScreen();
    createRecorder(stream);
  }
  return { startRecording };
}
