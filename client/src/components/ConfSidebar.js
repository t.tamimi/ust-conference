import React, { useRef } from "react";
import { chatMessages } from "../data/data";
import { useGlobalContext } from "../context/context";

import {
  BsMic,
  BsMicMute,
  BsCameraVideo,
  BsCameraVideoOff,
  BsSend,
} from "react-icons/bs";

export const ConfSidebar = () => {
  const {
    user,
    participants,
    session,
    socket,
    messages,
    isSideOpen,
    setSideOpen,
    participantContainerTab,
    participantTab,
    chatContainerTab,
    chatTab
  } = useGlobalContext();
  
  const messageRef = useRef();

  const toggleTab = (tabName) => {
    if (tabName === "participants") {
      participantTab.current.className = "active";
      chatTab.current.className = "";
      participantContainerTab.current.className = "active";
      chatContainerTab.current.className = "";
      return;
    }

    chatTab.current.className = "active";
    participantTab.current.className = "";
    chatContainerTab.current.className = "active";
    participantContainerTab.current.className = "";
  };

  const sendMessage = () => {
    const message = messageRef.current.value;
    if (message) {
      socket.current.emit("new-message", message);
      messageRef.current.value = "";
    }
  };

  return (
    <aside className={`session-sidebar ${isSideOpen ? 'open': ''}`}>
      <article>
        <ul>
          <li
            ref={participantTab}
            className="active"
            onClick={() => toggleTab("participants")}
          >
            Participants
          </li>
          <li ref={chatTab} onClick={() => toggleTab("chat")}>
            chat
          </li>
        </ul>
        <section
          id="participants-container"
          className="active"
          ref={participantContainerTab}
        >
          <ul>
            {participants &&
              participants.map((participant) => (
                <li key={participant.user.id}>
                  <span>{participant.user.name.substring(0, 1)}</span>
                  <span>
                    {participant.user.name}{" "}
                    {parseInt(participant.user.id) ===
                      parseInt(session.host.id) && "(Host)"}
                  </span>
                  <span>
                    <BsMic
                      color="var(--danger-color)"
                      style={{ cursor: "pointer", fontSize: "1.3rem", display: participant.controls.audio ? '' : 'none' }}
                    />
                    <BsMicMute
                      color="var(--danger-color)"
                      style={{ cursor: "pointer", fontSize: "1.3rem", display: participant.controls.audio ? 'none' : '' }}
                    />
                    <BsCameraVideo
                      color="var(--primary-color)"
                      style={{ cursor: "pointer", fontSize: "1.3rem", display: participant.controls.video ? '' : 'none' }}
                    />
                    <BsCameraVideoOff
                      color="var(--primary-color)"
                      style={{ cursor: "pointer", fontSize: "1.3rem", display: participant.controls.video ? 'none' : '' }}
                    />
                  </span>
                </li>
              ))}
          </ul>
        </section>
        <section id="chat-container" ref={chatContainerTab}>
          <ul>
            {messages &&
              messages.map((message, index) => (
                <li
                  key={index}
                  className={`${
                    parseInt(user.id) === parseInt(message.sender.id)
                      ? "me"
                      : ""
                  }`}
                >
                  <p>
                    <span>
                      {parseInt(user.id) === parseInt(message.sender.id)
                        ? "You"
                        : message.sender.name}
                    </span>
                    <span>{message.time}</span>
                  </p>
                  <p>{message.content}</p>
                </li>
              ))}
          </ul>
          <div>
            <input
              type="text"
              placeholder="Write your message"
              ref={messageRef}
            />
            <button type="button" onClick={sendMessage}>
              <BsSend />
            </button>
          </div>
        </section>
      </article>
    </aside>
  );
};
