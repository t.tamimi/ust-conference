import React, { useRef } from "react";
import { controlButtons } from "../data/data";
import { ControlButton } from "./ControlButton";
import { useParams, useNavigate } from "react-router-dom";
import { MdOutlineContentCopy } from "react-icons/md";
import { IoExitOutline } from "react-icons/io5";
import {
  coptyToClipbord,
  findElementBySelector,
  getAudioIcon,
  renderSharedScreen,
  saveItemToLocalStorage,
  toggleParticipantVideo,
} from "../helper/utils";
import { useGlobalContext } from "../context/context";
import Peer from "peerjs";
import { io } from "socket.io-client";
import useScreenRecorder from "../hooks/useScreenRecorder";

export const RigthSide = () => {
  const { id } = useParams();
  const sessionLink = `${window.location.origin}/${id}`;
  const {
    myStream,
    socket,
    constriants,
    setConstriants,
    isSideOpen,
    setSideOpen,
    participantContainerTab,
    participantTab,
    chatContainerTab,
    chatTab,
    participants,
    setParticipants,
    session,
    setView,
    view,
    user,
    URL,
    screenShareList,
    setScreenShareList,
  } = useGlobalContext();

  const shareSocket = useRef();

  const {startRecording} = useScreenRecorder({ filename: session._id, socket, cloudRecording: true })

  const changeStreamTrack = (type, status) => {
    let newConstraints = { ...constriants, [type]: status };

    if (type === "audio") {
      let participantAudioWrapper = findElementBySelector(
        `div.me`
      ).parentNode.querySelector(".participant-audio-status");

      participantAudioWrapper.innerHTML = getAudioIcon(newConstraints.audio);
    }

    myStream.current.getTracks().forEach((track) => {
      if (track.kind === type) {
        track.enabled = status;
      }
    });

    setConstriants(newConstraints);
    saveItemToLocalStorage("constriants", JSON.stringify(newConstraints));

    toggleParticipantVideo(newConstraints, "div.me");

    socket.current.emit("stream-changed", newConstraints);

    // change participants state
    const newParticipants = participants.map((participant) => {
      if (participant.socketId === socket.current.id) {
        participant.controls = newConstraints;
      }

      return participant;
    });

    console.log(newParticipants);

    setParticipants(newParticipants);

    socket.current.emit("participants-list-changed", newParticipants);
  };

  const toggleSidebar = (type) => {
    if (!isSideOpen) {
      setSideOpen(true);
    }

    if (type === "participants" && participantTab.current.className === "") {
      participantTab.current.className = "active";
      chatTab.current.className = "";
      participantContainerTab.current.className = "active";
      chatContainerTab.current.className = "";
      return;
    } else if (type === "chat" && chatTab.current.className === "") {
      chatTab.current.className = "active";
      participantTab.current.className = "";
      chatContainerTab.current.className = "active";
      participantContainerTab.current.className = "";
      return;
    }

    setSideOpen(false);
    chatTab.current.className = "";
    participantTab.current.className = "";
    participantContainerTab.current.className = "";
    chatContainerTab.current.className = "";
  };

  const endSession = () => {
    // myStream.current.getTracks().forEach(track => track.stop());

    socket.current.emit("session-ended");
  };

  const shareScreen = () => {
    const screenShareBtn = findElementBySelector('#btn-5');
    screenShareBtn.classList.add('btn-share');
    shareSocket.current = io(URL, {
      auth: {
        sessionId: session._id,
        user,
        isShareScreenSocket: true
      },
    });

    shareSocket.current.on("connect", () => {
      const sharePeer = new Peer(shareSocket.current.id, {
        path: "/peerjs",
        host: "/",
        port: "8000",
      });

      sharePeer.on("open", () => {
        shareSocket.current.emit("share-screen");
      });

      sharePeer.on("call", async (call) => {
        if(call.peer === socket.current.id) return;
        
        try {
          console.log(call, '9999999')
          const stream = await navigator.mediaDevices.getDisplayMedia({
            video: {
              displaySurface: "monitor",
            },
            audio: false,
          });

          stream.oninactive = () => {
            shareSocket.current.emit('screen-share-ended')
            screenShareBtn.classList.remove('btn-share');
            
            sharePeer.destroy();
            shareSocket.current.disconnect();
          };

          call.answer(stream);
          setView("screen-share");
          setScreenShareList([
            ...screenShareList,
            {
              remoteUserId: shareSocket.current.id,
              remoteUserName: user.name,
            },
          ]);
          renderSharedScreen(user, stream, shareSocket.current.id);
        } catch (error) {
          screenShareBtn.classList.remove('btn-share');
          shareSocket.current.emit('screen-share-canceled');
          sharePeer.destroy();
          shareSocket.current.disconnect();
        }
      });
    });
  };

  return (
    <React.Fragment>
      <article className={`video-container ${isSideOpen ? "open" : ""}`}>
        <section className={`video-stream ${view}`}>
          <section
            className={`participants-video ${
              view === "speaker" ? "snaps-inline" : ""
            }`}
          ></section>
          <section className="hero-stream"></section>
        </section>
        <section className="controls">
          <ControlButton
            Icon={MdOutlineContentCopy}
            text={`${id.substring(0, 10)}...`}
            bgColor=""
            action={coptyToClipbord(sessionLink)}
          />
          <div style={{ display: "flex", justifyContent: "center" }}>
            <ControlButton
              key={controlButtons[0].id}
              {...controlButtons[0]}
              display={constriants.audio ? "" : "none"}
              fontSize="1.5rem"
              action={() => changeStreamTrack("audio", false)}
            />
            <ControlButton
              key={controlButtons[1].id}
              {...controlButtons[1]}
              bgColor="var(--danger-color)"
              display={constriants.audio ? "none" : ""}
              fontSize="1.5rem"
              action={() => changeStreamTrack("audio", true)}
            />
            <ControlButton
              key={controlButtons[2].id}
              {...controlButtons[2]}
              display={constriants.video ? "" : "none"}
              fontSize="1.5rem"
              action={() => changeStreamTrack("video", false)}
            />
            <ControlButton
              key={controlButtons[3].id}
              {...controlButtons[3]}
              display={constriants.video ? "none" : ""}
              bgColor="var(--danger-color)"
              fontSize="1.5rem"
              action={() => changeStreamTrack("video", true)}
            />
            <ControlButton
              key={controlButtons[4].id}
              {...controlButtons[4]}
              fontSize="1.5rem"
              action={() => {
                shareScreen();
              }}
            />
            <ControlButton
              key={controlButtons[5].id}
              {...controlButtons[5]}
              fontSize="1.5rem"
              action={() => toggleSidebar("participants")}
            />
            <ControlButton
              key={controlButtons[7].id}
              {...controlButtons[7]}
              fontSize="1.5rem"
              action={() => { startRecording() }}
            />
            <ControlButton
              key={controlButtons[6].id}
              {...controlButtons[6]}
              fontSize="1.5rem"
              action={() => toggleSidebar("chat")}
            />
          </div>
          <ControlButton
            Icon={IoExitOutline}
            text={`leave`}
            bgColor="var(--danger-color)"
            action={endSession}
          />
        </section>
      </article>
    </React.Fragment>
  );
};
