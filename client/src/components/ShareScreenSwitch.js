import React, {useEffect, useState} from 'react'
import { useGlobalContext } from '../context/context';

export default function ShareScreenSwitch() {
  const {screenShareList}  = useGlobalContext();  
  const [currentScreenShare, setCurrentScreenShare] = useState(screenShareList[screenShareList.length - 1]);

  const switchScreenShare = (remoteUserId) => {
    if(remoteUserId === 0) return;
    const screenShare = screenShareList.find(share => share.remoteUserId === remoteUserId);
    setCurrentScreenShare(screenShare);

    const screenShareElements = document.querySelectorAll('.hero-stream div');
    screenShareElements.forEach(screenShareEl => {
        if(screenShareEl.dataset.peerId === remoteUserId) {
            screenShareEl.style.zIndex = '2'
        }else {
            screenShareEl.style.zIndex = '1'
        }
    })
    
  }

  useEffect(()=> {
    setCurrentScreenShare(screenShareList[screenShareList.length - 1])
  },[screenShareList])

  return (
    <section className='share-switcher'>
        <article>
            <h4>You are viewing {currentScreenShare.remoteUserName} screen</h4>
        </article>
        <article>
            <select value={currentScreenShare.remoteUserId} onChange={e => switchScreenShare(e.target.value)}>
                {/* <option key={0} value="0">view Options</option> */}
                {screenShareList && screenShareList.map(screenShare => (
                    <option key={screenShare.remoteUserId} value={screenShare.remoteUserId}>{screenShare.remoteUserName}</option>
                ))}
            </select>
        </article>
    </section>
  )
}
