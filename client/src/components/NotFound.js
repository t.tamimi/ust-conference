import React from 'react'

import notFoundImg from '../404.png'

export default function NotFoundPage() {
  return (
    <section className='not-found-wrapper'>
        <img src={notFoundImg} />
    </section>
  )
}
