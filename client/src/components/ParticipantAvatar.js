import React from "react";

const ParticipantAvatar = (props) => {
  const { name } = props;
  
  return (
    <section className="avater">
      <span>{name.substr(0,1)}</span>
    </section>
  );
};

export default ParticipantAvatar;
