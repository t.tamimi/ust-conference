import React from "react";
import Logo from "../logo.png";
import ActionButton from "./ActionButton";
import { MdOutlineContentCopy } from "react-icons/md";
import { IoExitOutline } from "react-icons/io5";
import ParticipantAvatar from "./ParticipantAvatar";
import { useGlobalContext } from "../context/context";
import ShareScreenSwitch from "./ShareScreenSwitch";

export const ConfHeader = () => {

  const { participants, screenShareList } = useGlobalContext();
  return (
    <header className="session-header">
      <img src={Logo} />
      <section>
        <h4>Design Deportment</h4>
        { screenShareList.length > 0 && <ShareScreenSwitch/>}
        <section>
          <article>
            <ActionButton
              title="Copy Link"
              Icon={MdOutlineContentCopy}
              colorClass="primary"
            />
            <ActionButton
              title="Leave"
              Icon={IoExitOutline}
              colorClass="danger"
            />
          </article>
          <article>
            {participants &&
              participants.slice(0,5).map((participant) => (
                <ParticipantAvatar key={participant.user.id} name={participant.user.name} />
              ))}
             {participants && participants.length > 5 && <span> +{participants.length - 5} Members </span>} 
          </article>
        </section>
      </section>
    </header>
  );
};
