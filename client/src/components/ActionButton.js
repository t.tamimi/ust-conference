import React from "react";

const ActionButton = (props) => {
  const { title, Icon, colorClass } = props;
  return (
    <section className={`action-wrapper ${colorClass}`}>
      <Icon />
      <span>{ title }</span>
    </section>
  );
};

export default ActionButton;
