import React from "react";

export const ControlButton = function(props) {
  const { Icon, bgColor, text='', fontSize='1.3rem', action=()=>{}, display='', id='' } = props;
  return (
    <span
      id={`btn-${id}`}
      className="control-btn"
      style={{
        backgroundColor: `${bgColor !== "" ? bgColor : "#2b2d2e"}`,
        color: '#fff',
        fontSize,
        display
      }}
      onClick={action}
    >
      <span style={{ marginRight: text !=='' ? 10 : 0 }}>{text}</span> 
      <Icon />
    </span>
  );
};
