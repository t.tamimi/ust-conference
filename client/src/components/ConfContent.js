import React from 'react'
import { ConfSidebar } from './ConfSidebar'
import { RigthSide } from './RigthSide'

export const ConfContent = () => {
  return (
      <section className='content-wrapper'>
        <RigthSide />
        <ConfSidebar />
      </section>
      
  )
}
