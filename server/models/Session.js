const mongoose = require('mongoose');

const sessionSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    host: {
        type: Map,
        of: String,
        required: true
    },
    status: Boolean,
    sesstionType: {
        type: String,
        required: true,
        trim: true,
        default: 'Normal'
    }
}, {
    timestamps: true
})


const Session = mongoose.model('Session', sessionSchema)

module.exports = Session;

