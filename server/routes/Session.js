const express = require("express");
const sessionController = require("../controllers/session");
// const auth = require("../middelwares/auth");
const router = new express.Router();

router.get("/sessions", sessionController.getSessionsController);
router.post("/sessions", sessionController.createSessionController);
router.get("/sessions/:id", sessionController.getSessionController);

module.exports = router;
