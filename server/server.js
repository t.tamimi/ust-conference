const express = require("express");
const bodyParser = require("body-parser");
const { createServer } = require("http");
const { Server } = require("socket.io");
const app = express();
const { ExpressPeerServer } = require("peer");
const sessionRouter = require("./routes/Session");
require("./db/mongoose");

// const cors = require('cors')

const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000",
  },
});

const peerServer = ExpressPeerServer(httpServer, {
  debug: true,
});

app.use(bodyParser.json());
app.use("/peerjs", peerServer);
app.use("/api", sessionRouter);

const peers = [];
const messages = [];
const screenShares = [];

io.on("connection", (socket) => {
  console.log("new connection" + socket.id);
  socket.join(socket.handshake.auth.sessionId);

  socket.on("ask-permission-to-join", (user) => {
    socket
      .to(socket.handshake.auth.sessionId)
      .emit("ask-permission-to-join", socket.handshake.auth.user);
  });

  socket.on("ask-permission-to-join-answer", (status, userId) => {
    socket
      .to(socket.handshake.auth.sessionId)
      .emit("ask-permission-to-join-answer", status, userId);
  });

  socket.on("user-connected", (id, constriants) => {
    socket
      .to(socket.handshake.auth.sessionId)
      .emit("user-connected", id, socket.handshake.auth.user);

    const roomIndex = peers.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    if (roomIndex != -1) {
      peers[roomIndex].participants.push({
        socketId: socket.id,
        user: socket.handshake.auth.user,
        controls: constriants,
      });

      io.to(socket.handshake.auth.sessionId).emit(
        "participants-list-changed",
        peers[roomIndex].participants
      );
      return;
    }

    peers.push({
      room: socket.handshake.auth.sessionId,
      participants: [
        {
          socketId: socket.id,
          user: socket.handshake.auth.user,
          controls: constriants,
        },
      ],
    });

    console.log(JSON.stringify(peers));

    io.to(socket.handshake.auth.sessionId).emit(
      "participants-list-changed",
      peers[peers.length - 1].participants
    );

    // console.log(peers);
  });

  socket.on("change-caller-info", (remotePeerId) => {
    if (!peers.find((peer) => peer.room === socket.handshake.auth.sessionId))
      return;

    const participants =
      peers.find((peer) => peer.room === socket.handshake.auth.sessionId)
        .participants || null;
    const constriants =
      participants.find((participant) => participant.socketId === socket.id) //TODO fix the bug here
        .controls || null;

    if (participants && constriants) {
      socket.to(socket.handshake.auth.sessionId).emit("change-caller-info", {
        recipentId: remotePeerId,
        callerName: socket.handshake.auth.user.name,
        callerId: socket.id,
        callerConstraints: constriants,
      });
    }
  });

  socket.on("new-message", (message) => {
    const roomIndex = messages.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    if (roomIndex != -1) {
      messages[roomIndex].messages.push({
        sender: socket.handshake.auth.user,
        content: message.trim(),
        time: Intl.DateTimeFormat("en", {
          hour: "numeric",
          minute: "numeric",
          hour12: true,
        }).format(new Date()),
      });

      io.to(socket.handshake.auth.sessionId).emit(
        "new-message",
        messages[roomIndex].messages
      );

      return;
    }

    messages.push({
      room: socket.handshake.auth.sessionId,
      messages: [
        {
          sender: socket.handshake.auth.user,
          content: message.trim(),
          time: Intl.DateTimeFormat("en", {
            hour: "numeric",
            minute: "numeric",
            hour12: true,
          }).format(new Date()),
        },
      ],
    });

    console.log(messages);

    io.to(socket.handshake.auth.sessionId).emit("new-message", [
      {
        sender: socket.handshake.auth.user,
        content: message.trim(),
        time: Intl.DateTimeFormat("en", {
          hour: "numeric",
          minute: "numeric",
          hour12: true,
        }).format(new Date()),
      },
    ]);
  });

  socket.on("disconnect", (reason) => {
    console.log(`User (${socket.id}) disconnected with reason ${reason}`);
    socket
      .to(socket.handshake.auth.sessionId)
      .emit("user-disconnected", socket.id);

    const roomIndex = peers.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    if (roomIndex != -1) {
      peers[roomIndex].participants = peers[roomIndex].participants.filter(
        (participant) => participant.socketId != socket.id
      );
    }

    if (!socket.handshake.auth.isShareScreenSocket) {
      io.to(socket.handshake.auth.sessionId).emit(
        "participants-list-changed",
        peers[roomIndex].participants
      );
    }
  });

  socket.on("participants-list-changed", (parts) => {
    const roomIndex = peers.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    console.log(parts);

    if (roomIndex != -1) {
      peers[roomIndex].participants = parts;
    }

    io.to(socket.handshake.auth.sessionId).emit(
      "participants-list-changed",
      parts
    );
  });

  socket.on("stream-changed", (constriants) => {
    socket
      .to(socket.handshake.auth.sessionId)
      .emit("stream-changed", constriants, socket.id);

    console.log(constriants);
  });

  socket.on("session-ended", () => {
    io.to(socket.handshake.auth.sessionId).emit(
      "session-ended",
      socket.handshake.auth.user.id
    );

    // end session in db
  });

  socket.on("share-screen", () => {
    const roomIndex = screenShares.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    if (roomIndex != -1) {
      screenShares[roomIndex].screenShares.push({
        remoteUserId: socket.id,
        remoteUserName: socket.handshake.auth.user.name,
      });

      socket
        .to(socket.handshake.auth.sessionId)
        .emit(
          "share-screen",
          socket.id,
          socket.handshake.auth.user,
          screenShares[roomIndex].screenShares
        );

      return;
    }

    screenShares.push({
      room: socket.handshake.auth.sessionId,
      screenShares: [
        {
          remoteUserId: socket.id,
          remoteUserName: socket.handshake.auth.user.name,
        },
      ],
    });

    socket
      .to(socket.handshake.auth.sessionId)
      .emit(
        "share-screen",
        socket.id,
        socket.handshake.auth.user,
        screenShares[screenShares.length - 1].screenShares
      );
  });

  socket.on("screen-share-ended", () => {
    const roomIndex = peers.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    const screenShareRoomIndex = screenShares.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    if (roomIndex != -1 && screenShareRoomIndex != -1) {
      screenShares[screenShareRoomIndex].screenShares = screenShares[
        screenShareRoomIndex
      ].screenShares.filter(
        (screenShare) => screenShare.remoteUserId != socket.id
      );

      socket
        .to(socket.handshake.auth.sessionId)
        .emit(
          "screen-share-ended",
          socket.id,
          peers[roomIndex].participants,
          screenShares[screenShareRoomIndex].screenShares
        );
    }
  });

  socket.on("screen-share-canceled", () => {
    const roomIndex = screenShares.findIndex(
      (peer) => peer.room == socket.handshake.auth.sessionId
    );

    if (roomIndex != -1) {
      screenShares[roomIndex].screenShares = screenShares[
        roomIndex
      ].screenShares.filter(
        (screenShare) => screenShare.remoteUserId != socket.id
      );

      socket
        .to(socket.handshake.auth.sessionId)
        .emit("screen-share-canceled", screenShares[roomIndex].screenShares);
    }
  });

  socket.on("new-video-chunk", (recordedCVideoBuffer) => {
    console.log(recordedCVideoBuffer);
  })
});

const PORT = process.env.PORT || 8000;

httpServer.listen(PORT, () => console.log("running on port " + PORT));
