const Session = require('../models/Session');
const { validateKeys } = require('../helpers/helper');
const getSessionsController = async (req, res) => {
    const sessions = await Session.find();
    res.json({sessions});
}

const createSessionController = async (req, res) => {
    const sessionData = req.body;
    if(validateKeys(sessionData, ['title', 'host', 'status', 'sessionType'])) {
        const session = new Session(req.body);
        await session.save();
        res.json({session_id: session._id});
        return;
    }
    res.json({error: "Some fields are messing..."})
}

const getSessionController = async (req, res) => {
    const sessionId = req.params.id;

    try {
        const session = await Session.findOne({_id: sessionId, status: true})
        if(session) {
            return res.json(session);
        }

        return res.status(404).json({ error: "Not found..." })
        
    } catch (error) {
        res.status(400).send({ error: "Some Think Went Wrong" });
    }
}

module.exports = {
    getSessionsController,
    createSessionController, 
    getSessionController
}