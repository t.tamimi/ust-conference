const validateKeys = (obj,keysArr) => {
    let validKeys = true;

    for (const key of keysArr) {
        if(!Object.keys(obj).includes(key)) {
            validKeys = false;
        }
    }
    return validKeys;
}

module.exports = {
    validateKeys
}